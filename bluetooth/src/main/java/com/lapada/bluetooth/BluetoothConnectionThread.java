package com.lapada.bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Looper;
import android.util.Log;

public class BluetoothConnectionThread implements Runnable
{
    private final String TAG = "BluetoothConnectThread";

    private BluetoothSocket mSocket;
    private BluetoothDevice remoteDevice;
    private BluetoothController controller;
    private InputStream inputStream;
    private OutputStream outputStream;

    public BluetoothConnectionThread(BluetoothDevice btDevice, BluetoothController controller)
    {
        this.remoteDevice = btDevice;

        BluetoothAdapter.getDefaultAdapter();

        this.controller = controller;
    }

    @Override
    public void run()
    {
        Looper.prepare();
        connect();

        try
        {
            mSocket.connect();
        }
        catch (IOException connectException)
        {
            try
            {
                mSocket.close();

                Log.i(TAG, "Erro ao conectar");
                Log.i(TAG, connectException.getMessage());
            }
            catch (IOException closeException)
            {
                Log.i(TAG, "Erro ao conectar");
            }
            return;
        }

        try
        {
            inputStream = mSocket.getInputStream();
            outputStream = mSocket.getOutputStream();
        }
        catch (IOException e)
        {
            Log.i(TAG, "Erro ao pegar inputStream");
        }

        BluetoothInputThread inputThread = new BluetoothInputThread(inputStream, controller);
        new Thread(inputThread).start();

        BluetoothOutputThread outputThread = new BluetoothOutputThread(outputStream, controller);
        new Thread(outputThread).start();

        controller.setConnected(true);

        Log.i(TAG, "Deu certo conectar!!!");
    }

    private void connect() {
        BluetoothSocket tmp = null;

        try {
            Method m = remoteDevice.getClass().getMethod("createRfcommSocket",new Class[] { int.class });
            tmp = (BluetoothSocket) m.invoke(remoteDevice, Integer.valueOf(1));

        }
        catch (Exception e)
        {
            Log.i(TAG,"falha na criaçăo do socket");
        }
        mSocket = tmp;
    }

    public BluetoothSocket getSocket()
    {
        return mSocket;
    }

    public InputStream getInputStream()
    {
        return inputStream;
    }
}