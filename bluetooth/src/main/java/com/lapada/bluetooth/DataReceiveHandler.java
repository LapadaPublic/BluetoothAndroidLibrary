package com.lapada.bluetooth;

import java.util.Scanner;

public interface DataReceiveHandler
{
    public void scannerInitialized(Scanner scanner);
    public void dataArrived(Scanner scanner);
}
