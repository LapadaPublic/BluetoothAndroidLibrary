package com.lapada.bluetooth;

import java.util.Scanner;

public class BluetoothControllerForUnity
{
    public BluetoothController bluetoothController;

    public boolean hasNextByte = false;

    public boolean hasNextLine = false;

    private Scanner mScanner;

    public BluetoothControllerForUnity()
    {
        bluetoothController = new BluetoothController(new DataReceiveHandler()
        {
            @Override
            public void scannerInitialized(Scanner scanner)
            {
                mScanner = scanner;
            }

            @Override
            public void dataArrived(Scanner scanner)
            {
                hasNextByte = scanner.hasNextByte();
                hasNextLine = scanner.hasNextLine();
            }
        });
    }

    public int getNextByte()
    {
        return mScanner.nextByte();
    }

    public String getNextLine()
    {
        return mScanner.nextLine();
    }

    public String[] getPairedDevicesNames()
    {
        return bluetoothController.getBondedDevicesNames();
    }

    public void connectToDevice(int deviceIndex)
    {
        bluetoothController.connectWithDevice(deviceIndex);
    }

    public void connectToDevice(String deviceName)
    {
        bluetoothController.connectWithDevice(deviceName);
    }

    public void sendByte(int byteToSend)
    {
        bluetoothController.sendByte((byte)byteToSend);
    }

    public void sendString(String stringToSend)
    {
        bluetoothController.sendString(stringToSend);
    }
}
