package com.lapada.bluetooth;

import java.io.InputStream;
import java.util.Scanner;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

public class BluetoothInputThread implements Runnable
{
    private final String TAG = "BluetoothInputThread";

    private InputStream inputStream;
    private BluetoothController controller;
    private boolean canceled = false;
    private Handler handler;
    private Scanner scanner;

    public BluetoothInputThread(InputStream inputStream, BluetoothController controller)
    {
        this.inputStream = inputStream;
        this.controller = controller;
        handler = new Handler(controller);
        scanner = new Scanner(inputStream);
    }

    public InputStream getInputStream()
    {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream)
    {
        this.inputStream = inputStream;
    }

    @Override
    public void run()
    {
        Looper.prepare();

        //I significa que o scanner foi inicializado
        Message.obtain(handler, 'I', scanner).sendToTarget();

        while (!canceled)
        {
            if (scanner.hasNextByte())
            {
                //D significa que recebeu um dado
                Message.obtain(handler, 'D', scanner).sendToTarget();
            }
        }
    }

    public Handler gethandler()
    {
        return handler;
    }

    public void sethandler(Handler mHandler)
    {
        this.handler = mHandler;
    }

    public void cancel()
    {
        canceled = true;
    }
}
