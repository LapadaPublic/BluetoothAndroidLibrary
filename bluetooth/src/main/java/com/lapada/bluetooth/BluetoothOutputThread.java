package com.lapada.bluetooth;

import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Queue;

public class BluetoothOutputThread implements Runnable
{
    private final String TAG = "BtOutputThread";

    private Queue<Byte> sendQueue;
    private OutputStream outputStream;
    private boolean canceled = false;

    public BluetoothOutputThread(OutputStream outputStream, BluetoothController controller)
    {
        this.sendQueue = controller.getSendQueue();
        this.outputStream = outputStream;
    }

    @Override
    public void run()
    {
        while (!canceled)
        {
            if (!sendQueue.isEmpty())
            {
                try
                {
                    outputStream.write((int) sendQueue.poll());
                }
                catch (IOException e)
                {
                    Log.e(TAG, "Erro ao enviar byte");
                }
            }
        }
    }

    public void Cancel()
    {
        canceled = true;
    }
}
