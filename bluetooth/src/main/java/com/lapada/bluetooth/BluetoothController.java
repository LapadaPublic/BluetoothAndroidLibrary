package com.lapada.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.os.Handler.Callback;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

public class BluetoothController implements Callback
{
    private String TAG = "BluetoothController";
    private BluetoothDevice[] bondedDevices;
    private BluetoothConnectionThread connectionThread;
    private boolean isConnected = false;
    private DataReceiveHandler receiveHandler;
    private Queue<Byte> sendQueue;

    public BluetoothController(DataReceiveHandler receiveHandler)
    {
        sendQueue = new LinkedBlockingQueue<>();
        this.receiveHandler = receiveHandler;
        Looper.prepare();
        bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices().toArray(new BluetoothDevice[0]);
    }

    public String[] getBondedDevicesNames()
    {
        String[] deviceNames = new String[bondedDevices.length];

        for (int i = 0; i < bondedDevices.length; i++)
        {
            deviceNames[i] = bondedDevices[i].getName();
            Log.i(TAG, "dispositivo pareado: " + deviceNames[i]);
        }
        return deviceNames;
    }

    public void connectWithDevice(int deviceIndex)
    {
        connectionThread = new BluetoothConnectionThread(bondedDevices[deviceIndex], this);
        new Thread(connectionThread).start();
    }

    public void connectWithDevice(String deviceName)
    {
        for (BluetoothDevice device : bondedDevices)
        {
            if (device.getName().equalsIgnoreCase(deviceName))
            {
                connectionThread = new BluetoothConnectionThread(device, this);
                new Thread(connectionThread).start();
                return;
            }
        }
        throw new IllegalArgumentException("Não há um dispositivo pareado com esse nome");
    }

    public void sendByte(byte byteToSend)
    {
        sendQueue.add(byteToSend);
    }

    public void sendString(String stringToSend)
    {
        for (char character : stringToSend.toCharArray())
        {
            sendQueue.add((byte) character);
        }
    }

    public Queue<Byte> getSendQueue()
    {
        return sendQueue;
    }

    public boolean isItConnected()
    {
        return isConnected;
    }

    public void setConnected(boolean connected)
    {
        isConnected = connected;
    }


    @Override
    public boolean handleMessage(Message msg)
    {
        Log.i(TAG, "O handler recebeu a msg");

        int buttonState;

        switch (msg.what)
        {
            case 'I':
                receiveHandler.scannerInitialized((Scanner) msg.obj);
            case 'D':
                receiveHandler.dataArrived((Scanner) msg.obj);
            default:
                break;
        }
        return false;
    }

}